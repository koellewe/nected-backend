DROP PROCEDURE IF EXISTS generate_closest_matches_single;
CREATE PROCEDURE generate_closest_matches_single(IN reqPID VARCHAR(255))
  BEGIN

    UPDATE matching_statuses SET status = 1 WHERE profileID = reqPID;

    DELETE FROM matches
    WHERE matcher = reqPID AND legit_match = 0;

    INSERT INTO matches

      SELECT reqPID, profileID, ROUND(20+RAND()*20, 2), 0
      FROM (
        SELECT profileID FROM users
          WHERE profileID NOT IN (
            SELECT matchee FROM matches
            WHERE matcher = reqPID AND legit_match = 1
          )
        ORDER BY RAND()
        LIMIT 5
      ) as tbl_random;

    UPDATE matching_statuses SET status = 0 WHERE profileID = reqPID;

  END;