DROP FUNCTION IF EXISTS user_exists;
CREATE FUNCTION user_exists(pProfileID VARCHAR(255))
  RETURNS TINYINT(1)
  BEGIN

    DECLARE pCount INTEGER DEFAULT 0;

    SELECT COUNT(profileID) INTO pCount
    FROM users WHERE profileID = pProfileID;

    RETURN pCount;

  END;
