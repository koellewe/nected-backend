DROP PROCEDURE IF EXISTS generate_matches_single;
CREATE PROCEDURE generate_matches_single(IN reqPID VARCHAR(255))
  BEGIN

    DECLARE pCount INT DEFAULT 0;

    UPDATE matching_statuses SET status = 1 WHERE profileID = reqPID;

    DELETE FROM matches
    WHERE matcher = reqPID AND legit_match = 1;

    INSERT INTO matches

      SELECT reqPID, profileID, quality, 1
      FROM (
             SELECT
               profileID,
               matchability(reqPID, profileID, matching_likes) AS quality
             FROM (
                    SELECT
                      likes.profileID,
                      COUNT(pageID) AS matching_likes
                    FROM likes
                      INNER JOIN
                      (
                        SELECT profileID
                        FROM (
                               SELECT profileID
                               FROM (

                                      SELECT profileID
                                      FROM users
                                      WHERE profileID NOT IN (
                                        SELECT ignoree
                                        FROM ignores
                                        WHERE ignorer = reqPID
                                      )
                                            AND users.profileID != reqPID

                                    ) as tbl_unignored
                               WHERE is_matchable(reqPID, tbl_unignored.profileID)
                             ) as tbl_matched_forward
                        WHERE is_matchable(tbl_matched_forward.profileID, reqPID)
                      ) as tbl_matched_backward
                        ON likes.profileID = tbl_matched_backward.profileID
                    GROUP BY likes.profileID
                  ) AS tbl_matching_likes
           ) AS tbl_matching_quality
      WHERE quality > get_setting(reqPID, 'MATCH_QUALITY')
      ORDER BY quality
      LIMIT 5;

    SELECT COUNT(matchee) INTO pCount
      FROM matches WHERE matcher = reqPID AND legit_match = 1;

    IF pCount >= 3 THEN
      DELETE FROM matches WHERE legit_match = 0 AND matcher = reqPID;
      UPDATE matching_statuses SET status = 0 WHERE profileID = reqPID;
    ELSE
      CALL generate_closest_matches_single(reqPID);
    END IF;

  END;