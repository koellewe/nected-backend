DROP FUNCTION IF EXISTS is_matchable;
CREATE FUNCTION is_matchable(matcherPID VARCHAR(255), matcheePID VARCHAR(255))
  RETURNS TINYINT(1)
  BEGIN

    DECLARE min_age INTEGER DEFAULT 18;

    DECLARE max_age INTEGER DEFAULT 9999;

    DECLARE begotten_dob DATE;

    DECLARE is_registered BOOLEAN DEFAULT FALSE;

    IF (matcherPID <> '' AND matcheePID <> '' AND matcherPID IS NOT NULL AND matcheePID IS NOT NULL) THEN

      SET min_age =
      get_setting(matcherPID, 'MIN_AGE');

      SET max_age =
      get_setting(matcherPID, 'MAX_AGE');

      SET is_registered = FALSE;

      IF max_age >= 80 THEN
        SET max_age = 9999;
      END IF;


      SELECT DoB, registered INTO begotten_dob, is_registered
      FROM users
      WHERE users.profileID = matcheePID;

      -- note: ignores handled one level up (get_matches) for efficiency

      -- matchability also

      RETURN

             FLOOR(DATEDIFF(NOW(), begotten_dob) / 365.25) BETWEEN min_age AND max_age

             AND within_distance(matcherPID, matcheePID)

             AND gender_matches(matcherPID, matcheePID)

             AND is_registered = TRUE;

    ELSE

      RETURN FALSE;

      END IF;

  END;
