<?php
/**
 * Created by PhpStorm.
 * User: koell
 * Date: 13 Mar 2018
 * Time: 6:07 PM
 */

require_once 'utils/funcs.php';

$outputArr = array();

$input = json_decode(file_get_contents("php://input"));

/**
 * input JSON:
 *
 *  action:
 *      set_messenger
 *          rauth: string
 *          messengerUsername: string
 *      finish_registration
 *          profileID: string
 *          output JSON:
 *              *failMsg: "USER_NOT_READY"
 *              *issues: JSONObject
 *              rauth: string
 *      register:
 *          accessToken: string
 *          *forceGender: char
 *          output JSON:
 *              *user_readiness: JSONObject
 *              rauth: string
 *              user_registered: boolean
 *      update_likes:
 *          accessToken: string
 *      get_user_info:
 *          rauth: string
 *          output JSON:
 *              user: JSONObject
 *      deregister:
 *          rauth: string
 *      update_location:
 *          rauth: string
 *          coords: JSONObject:
 *              x: double
 *              y: double
 *      register|update_location:
 *          accessToken: string
 *          *forceGender: char
 *          coords: JSONObject:
 *              x: double
 *              y: double
 *          output JSON:
 *              *user_readiness: JSONObject
 *              rauth: string
 *              user_registered: boolean
 *
 */

function main($action){

    global $input, $db, $outputArr;

    if ($action == 'set_messenger') {
        // checks whether username exists (how messenger.com reacts to it)
        // and then sets it as messengerUsername of user

        $profileID = validate_rauth($input->rauth);
        if ($profileID == false) {
            $outputArr['success'] = false;
            $outputArr['failMsg'] = 'BAD_RAUTH';
            return;
        }

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13'); //shh
        curl_setopt($ch, CURLOPT_URL, 'https://m.me/' . $input->messengerUsername);
        $out = curl_exec($ch);

        // line endings is the wonkiest piece of this whole thing
        $out = str_replace("\r", "", $out);

        // only look at the headers
        $headers_end = strpos($out, "\n\n");
        if ($headers_end !== false) {
            $out = substr($out, 0, $headers_end);
        }

        $headers = explode("\n", $out);
        $redir = '';
        foreach ($headers as $header) {
            if (substr($header, 0, 10) == "Location: ") {
                $target = substr($header, 10);

                $redir = $target;
                break;
            }
        }

        if (strpos($redir, '/t/') != false) {

            $stmt = $db->prepare('SELECT messengerUsername FROM users WHERE messengerUsername = ?
                                     AND profileID <> ?');
            $stmt->bind_param('ss', $input->messengerUsername, $profileID);
            $stmt->execute();

            if ($stmt->fetch()) {
                $outputArr['success'] = false;
                $outputArr['failMsg'] = 'USERNAME_ALREADY_REGISTERED';

            } else {
                set_messenger($profileID, $input->messengerUsername);
                $outputArr['success'] = true;
            }

        } else {
            $outputArr['success'] = false;
            $outputArr['failMsg'] = 'BAD_USERNAME';
        }

    } else if ($action == 'finish_registration') {

        $user_readiness = get_user_readiness($input->profileID);

        if ($user_readiness['ready']) {

            set_user_registered($input->profileID, true);

            $outputArr['success'] = true;
            $outputArr['rauth'] = get_rauth($input->profileID);

        } else {
            $outputArr['success'] = false;
            $outputArr['failMsg'] = 'USER_NOT_READY';
            $outputArr['user_readiness'] = $user_readiness;
        }


    } else if ($action == 'register') {

        require_once 'vendor/autoload.php';

        try {

            $fb = new \Facebook\Facebook([
                'app_id' => '1290687457743576',
                'app_secret' => '0e07f8a28942c79cd36c12f2bc64a08b',
                'default_graph_version' => 'v2.10',
                'default_access_token' => $input->accessToken
            ]);

            $response = $fb->get('/me?fields=first_name,last_name,email,birthday,gender');
            $userNode = $response->getGraphUser();
            $profileID = $userNode->getId();

            if ($userNode->getBirthday() == null && !user_exists($profileID)) {
                $outputArr['success'] = false;
                $outputArr['failMsg'] = 'BIRTHDAY_REQUIRED';
            } else {

                $gender = $userNode->getGender();

                if (isset($input->forceGender)) {
                    $gender = $input->forceGender;

                } else if ($gender == 'male') {
                    $gender = 'M';
                } elseif ($gender == 'female') {
                    $gender = 'F';
                } else {
                    $gender = 'O';
                }

                if (!user_exists($profileID)) {
                    insert_user($profileID, $userNode->getFirstName(), $userNode->getLastName(), $userNode->getEmail(),
                        $userNode->getBirthday()->format("Y-m-d"), $gender);
                }

                $response = $fb->get(
                    "/$profileID/likes"
                );

                $pagesEdge = $response->getGraphEdge();
                $jPages = json_decode($pagesEdge->asJson());

                if (sizeof($jPages) > 0) {

                    insert_likes_tmp($profileID, $jPages);

                    while ($nextPages = $fb->next($pagesEdge)) {
                        insert_likes_tmp($profileID, json_decode($nextPages->asJson()));
                        $pagesEdge = $nextPages;
                    }

                    update_likes($profileID);

                }

                $userReadiness = get_user_readiness($profileID);
                if ($userReadiness['ready']) {
                    $outputArr['success'] = true;
                } else {
                    $outputArr['success'] = false;
                    $outputArr['failMsg'] = 'USER_NOT_READY';
                    $outputArr['user_readiness'] = $userReadiness;
                }
                $outputArr['user_registered'] = is_registered($profileID);
                $outputArr['rauth'] = get_rauth($profileID);

            }


        } catch (Facebook\Exceptions\FacebookSDKException $e) {

            $outputArr['success'] = false;
            $outputArr['failMsg'] = 'Facebook SDK returned an error: ' . $e->getMessage();
            return;

        }

    } else if ($action == 'update_likes') {

        require_once 'vendor/autoload.php';

        try {

            $fb = new \Facebook\Facebook([
                'app_id' => '1290687457743576',
                'app_secret' => '0e07f8a28942c79cd36c12f2bc64a08b',
                'default_graph_version' => 'v2.10',
                'default_access_token' => $input->accessToken
            ]);

            $response = $fb->get('/me');
            $userNode = $response->getGraphUser();
            $profileID = $userNode->getId();

            $response = $fb->get(
                "/$profileID/likes"
            );

            $pagesEdge = $response->getGraphEdge();
            $jPages = json_decode($pagesEdge->asJson());

            if (sizeof($jPages) > 0) {

                insert_likes_tmp($profileID, $jPages);

                while ($nextPages = $fb->next($pagesEdge)) {
                    insert_likes_tmp($profileID, json_decode($nextPages->asJson()));
                    $pagesEdge = $nextPages;
                }

                update_likes($profileID);

            }

            $outputArr['success'] = true;
            $outputArr['likes'] = get_total_pages($profileID);

        } catch (Facebook\Exceptions\FacebookSDKException $e) {

            $outputArr['success'] = false;
            $outputArr['failMsg'] = 'Facebook SDK returned an error: ' . $e->getMessage();
            return;

        }

    } else if ($action == 'get_user_info') {

        $profileID = validate_rauth($input->rauth);
        if ($profileID == false){
            $outputArr['success'] = false;
            $outputArr['failMsg'] = 'BAD_RAUTH';
            return;
        }

        $outputArr['success'] = true;
        $outputArr['user'] = get_user_info($profileID);

    } else if ($action == 'deregister') {

        $profileID = validate_rauth($input->rauth);
        if ($profileID == false){
            $outputArr['success'] = false;
            $outputArr['failMsg'] = 'BAD_RAUTH';
            return;
        }

        set_user_registered($profileID, false);
        del_rauth($profileID);

        $outputArr['success'] = true;

    } else if ($action == 'update_location') {

        $profileID = validate_rauth($input->rauth);
        if ($profileID == false){
            $outputArr['success'] = false;
            $outputArr['failMsg'] = 'BAD_RAUTH';
            return;
        }

        new_loc($profileID, $input->coords->x, $input->coords->y);

        $outputArr['success'] = true;


    } else if ($action == 'register|update_location') {

        require_once 'vendor/autoload.php';

        try {

            $fb = new \Facebook\Facebook([
                'app_id' => '1290687457743576',
                'app_secret' => '0e07f8a28942c79cd36c12f2bc64a08b',
                'default_graph_version' => 'v2.10',
                'default_access_token' => $input->accessToken
            ]);

            $response = $fb->get('/me?fields=first_name,last_name,email,birthday,gender');
            $userNode = $response->getGraphUser();
            $profileID = $userNode->getId();

            if ($userNode->getBirthday() == null && !user_exists($profileID)) {
                $outputArr['success'] = false;
                $outputArr['failMsg'] = 'BIRTHDAY_REQUIRED';
            } else {

                $gender = $userNode->getGender();

                if (isset($input->forceGender)) {
                    $gender = $input->forceGender;

                } else if ($gender == 'male') {
                    $gender = 'M';
                } elseif ($gender == 'female') {
                    $gender = 'F';
                } else {
                    $gender = 'O';
                }

                if (!user_exists($profileID)) {
                    insert_user($profileID, $userNode->getFirstName(), $userNode->getLastName(), $userNode->getEmail(),
                        $userNode->getBirthday()->format("Y-m-d"), $gender);
                }

                $response = $fb->get(
                    "/$profileID/likes"
                );

                $pagesEdge = $response->getGraphEdge();
                $jPages = json_decode($pagesEdge->asJson());

                if (sizeof($jPages) > 0) {

                    insert_likes_tmp($profileID, $jPages);

                    while ($nextPages = $fb->next($pagesEdge)) {
                        insert_likes_tmp($profileID, json_decode($nextPages->asJson()));
                        $pagesEdge = $nextPages;
                    }

                    update_likes($profileID);

                }

                new_loc($profileID, $input->coords->x, $input->coords->y);

                $userReadiness = get_user_readiness($profileID);
                if ($userReadiness['ready']) {
                    $outputArr['success'] = true;
                } else {
                    $outputArr['success'] = false;
                    $outputArr['failMsg'] = 'USER_NOT_READY';
                    $outputArr['user_readiness'] = $userReadiness;
                }
                $outputArr['user_registered'] = is_registered($profileID);
                $outputArr['rauth'] = get_rauth($profileID);

            }


        } catch (Facebook\Exceptions\FacebookSDKException $e) {

            $outputArr['success'] = false;
            $outputArr['failMsg'] = 'Facebook SDK returned an error: ' . $e->getMessage();
            return;

        }


    } else {
        $outputArr['success'] = false;
        $outputArr['failMsg'] = 'Unsupported action';
    }

}

main($input->action);
echo json_encode($outputArr);
$db->close();