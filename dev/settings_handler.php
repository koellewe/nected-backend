<?php
/**
 * Created by PhpStorm.
 * User: koell
 * Date: 29 Jan 2018
 * Time: 9:32 AM
 */

require_once 'utils/funcs.php';

$outputArr = array();

$input = json_decode(file_get_contents("php://input"));

/**
 * Input json:
 *
 * rauth: string
 * action: String [save, get]
 * settings: array
 *  setting_key
 *  value
 */

if (isset($input->action) && isset($input->rauth)){

    $profileID = validate_rauth($input->rauth);
    if ($profileID == false){
        $outputArr['success'] = false;
        $outputArr['failMsg'] = 'BAD_RAUTH';

    }else if ($input->action == 'get'){

        $outputArr['success'] = true;
        $outputArr['settings'] = get_settings($profileID);

    }else if ($input->action == 'save' && isset($input->settings)){

        save_settings($profileID, $input->settings);
        $outputArr['success'] = true;

    }else{
        $outputArr['success'] = false;
        $outputArr['failMsg'] = 'check your params. Some are bad';
    }

}else{

    $outputArr['success'] = false;
    $outputArr['failMsg'] = 'action || profileID not supplied';

}

echo json_encode($outputArr);
$db->close();