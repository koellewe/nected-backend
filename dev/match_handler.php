<?php
/**
 * Created by PhpStorm.
 * User: koell
 * Date: 29 Jan 2018
 * Time: 1:11 PM
 */

require_once 'utils/funcs.php';

$outputArr = array();

$input = json_decode(file_get_contents("php://input"));

/**
 * input JSON:
 * rauth: string
 * action: refresh | retrieve
 * ouput JSON:
 *  *matches: JSONArray
 */

if (isset($input->rauth)){

    $profileID = validate_rauth($input->rauth);
    if ($profileID == false){
        $outputArr['success'] = false;
        $outputArr['failMsg'] = 'BAD_RAUTH';

    }else if ($input->action == 'refresh') {

        if (is_matching($profileID)){
            $outputArr['success'] = true;
            $outputArr['msg'] = 'MATCHING_ALREADY_STARTED';

        }else{
            $outputArr['success'] = true;
            $outputArr['msg'] = 'MATCHING_STARTED';
            echo json_encode($outputArr);

            generate_matches_single($profileID);
            $db->close();
            exit();

        }

    }else if ($input->action == 'retrieve') {

        if (is_matching($profileID)){
            $outputArr['success'] = false;
            $outputArr['msg'] = 'MATCHING_BUSY';

        }else{

            $matches = get_matches($profileID);

            $outputArr['matches'] = $matches;
            $outputArr['success'] = true;
        }

    }else{
        $outputArr['success'] = false;
        $outputArr['failMsg'] = 'BAD_ACTION';
    }

}else{
    $outputArr['success'] = false;
    $outputArr['failMsg'] = 'BAD_RAUTH';
}

echo json_encode($outputArr);
$db->close();