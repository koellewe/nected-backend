<?php
/**
 * Created by PhpStorm.
 * User: koell
 * Date: 06 Feb 2018
 * Time: 9:49 AM
 */

require_once '../utils/db_dev.php';
require_once '../utils/funcs.php';

set_time_limit(60*60);
ignore_user_abort(true);

function nice_time(DateTime $start, DateTime $end){

    $diff = date_diff($start, $end, true);

    $nice = '';

    if ($diff->d > 0)
        $nice .= $diff->d . 'd ';

    if ($diff->h > 0)
        $nice .= $diff->h . 'h ';

    if ($diff->s > 0)
        $nice .= $diff->s . 's';

    return trim($nice);
}

if ($_GET['admin_auth'] != 'Vu2xJCGzLAy4'){
    echo '<h1>ERROR: bad auth</h1>';
    exit();
}

echo '<p>Generating matches for all users. This is going to take a while...</p>';

$result = $db->query('SELECT profileID FROM users');

if ($result == false){
    echo 'Something went wrong.';
}else{

    $start_time = new DateTime();

    $stmt = $db->prepare('CALL generate_matches_single(?)');

    $usr_num = 0;
    while (($user = $result->fetch_assoc()) != null){

        $stmt->bind_param('s', $user['profileID']);
        $stmt->execute();
        //tests show this takes about 18s in a fully-compatible user set of 1000

        $usr_num++;

    }

    $stmt->close();

    $end_time = new DateTime();

    echo '<p>Done.</p>';
    echo '<p>processed '.$usr_num.' users in '.nice_time($start_time, $end_time).'</p>';

    $r = $db->query('SELECT COUNT(matchee) as matches_generated FROM matches');
    $a = $r->fetch_assoc();

    echo '<p>generated '.$a['matches_generated'].' matches</p>';

}
