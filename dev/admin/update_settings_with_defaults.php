<?php
/**
 * Created by PhpStorm.
 * User: koell
 * Date: 06 Feb 2018
 * Time: 9:49 AM
 */

require_once '../utils/db_dev.php';
require_once '../utils/funcs.php';

if ($_GET['admin_auth'] != 'Vu2xJCGzLAy4'){
    echo '<h1>ERROR: bad auth</h1>';
    exit();
}

$result = $db->query('SELECT profileID FROM users');

if ($result == false){
    echo 'Something went wrong.';
}else{

    while (($user = $result->fetch_assoc()) != null){

        add_new_default_settings($user['profileID']);

    }

    echo 'Done.';

}
