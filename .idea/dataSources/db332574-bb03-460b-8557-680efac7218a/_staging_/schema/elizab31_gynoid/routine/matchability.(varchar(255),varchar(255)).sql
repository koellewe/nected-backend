DROP FUNCTION matchability;
CREATE FUNCTION matchability(matcherPID VARCHAR(255), matcheePID VARCHAR(255))
  RETURNS DOUBLE
  BEGIN

    DECLARE pMatches INTEGER DEFAULT 0;
    DECLARE fMatchability, ageRelation, commonPerc DOUBLE DEFAULT 0.0;
    DECLARE rowCount INTEGER DEFAULT 0;
    DECLARE matcherAge, matcheeAge, ageDiff INTEGER DEFAULT 0;

    DROP TEMPORARY TABLE IF EXISTS tmp_matchee;
    CREATE TEMPORARY TABLE tmp_matchee (page_matches INTEGER PRIMARY KEY) ENGINE=MEMORY; -- should have either 0 or 1 records


    INSERT INTO tmp_matchee

    SELECT COUNT(likes.pageID) as page_matches

    FROM likes

    WHERE pageID IN (

      SELECT pageID FROM likes WHERE likes.profileID = matcherPID

    )

    GROUP BY likes.profileID

    HAVING likes.profileID = matcheePID;

    SELECT COUNT(page_matches) INTO rowCount
    FROM tmp_matchee;

    IF rowCOUNT = 1 THEN

      SELECT FLOOR(DATEDIFF(NOW(), users.DoB) / 365.25) INTO matcherAge
      FROM users WHERE profileID = matcherPID;

      SELECT FLOOR(DATEDIFF(NOW(), users.DoB) / 365.25) INTO matcheeAge
      FROM users WHERE profileID = matcheePID;

      SELECT page_matches INTO pMatches
        FROM tmp_matchee;

      SET commonPerc = hardcap(pMatches/get_total_likes(matcherPID) * 10, 1.0);
      -- realistically speaking, 10% likes matches is the max

      SET ageDiff = hardcap( ABS(matcherAge-matcheeAge) , 72);
      SET matcherAge = hardcap(matcherAge, 80);

      SET ageRelation = 60 * POW(2, 18/72) * POW(2, 0-(matcherAge/72));
      -- when age 18, ageDiff weighs 60%
      -- when age 80, ageDiff weighs 30%

      SET fMatchability = (commonPerc * (100-ageRelation)) + (((72-ageDiff)/72) * ageRelation);

      IF fMatchability > 98.0 THEN
        SET fMatchability = 98.0;
      ELSEIF fMatchability < 0.0 THEN
        SET fMatchability = 0;
      END IF;

      RETURN fMatchability;

    ELSE
      RETURN 0;
    END IF;
  END;
