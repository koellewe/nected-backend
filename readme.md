# Nected Backend

This is pretty much everything that made up the heart of the Nected Smart Matching service. Due to Facebook's policy changes in April 2018, Nected can no longer function and so the project has been abandoned. All code in this repo is unlicensed and open to public use.

## Matching
Before we jump into the nitty gritties of how we do matching, here's some terminology we use in the description of the algos:

- ***Match***: A user who is likely to be a good, compatible romantic or non-romantic partner for a specific user.
- ***Matcher***: A user who the backend is currently finding matches for.
- ***Matchee***: A candidate match for the matcher. It is however possible, that the matchee will not be a match. The suffix "ee" is based on "interviewer/interviewee".
- ***Filter for X***: In a list, filter *out* all items where their X property is false. Think of this as `WHERE X = TRUE` in SQL.
- ***Hardcap Y at Z***: If a number Y is greater than Z, set it to Z. This essentially makes Z the max for Y.
- ***Percentage (int)***: An integer between 0 and 100 that represents a percentage.
- ***Percentage (float)***: A float between 0 and 1 that represents a percentage.

Broadly speaking, Nected matches users by taking their **1. Facebook likes** and **2. Preferences** into account and running queries based on this data. Nected uses some seriously cool and complex methods for matching people. In a few big SQL queries, the backend generates up to 10 matches for a given user. It does this by taking all the users and filtering them, one property at a time. This list is capped at 5 users. If fewer than 3 candidates are found, the backend generates up to 5 pseudo-matches, called "close matches". Here is the whole method of getting matches for one user broken down, in order:

1. Get all users, and filter out ignored users. An ignored user is a matchee that the matcher has already matched with and has marked as "done". See `/feedback` endpoint.
2. Filter for forward matches. A forward match is a matchee that satisfies the following:
    - The age of the matchee is in the matcher's preferred age range (defined by his settings)
    - The matchee's location is within the matcher's preferred maximum radius. Both the matcher and matchee's most recent locations are used. If the matchee has no locations posted, they will not be able to match.
    - The matchee's gender is one the matcher is interested in. (also, his settings)
    - The matchee is registered. See `/users/registration`
3. Filter for backward matches. A backward match is a matchee whose settings are set in a way that the matcher satisfies the above conditions. This means that all matches require that the both parties fit in each other's settings.
4. Filter for match quality. This is actually the most complex and resource-intensive of all the steps. A match quality percentage is assigned to every matchee and matchees with a percentage below the matcher's minimum, are filtered out. Match qualities are calculated in the following way:
    1. The fb pages the matchee has liked is compared with the pages the matcher has liked. The number of page matches is divided by the total number of matcher page likes and then multiplied by 10 and finally hardcapped at 1.0. This will result in a percentage (float) that describes the amount of pages the matcher and matchee have in common, but assuming a theoretical maximum of 10%. This means a 6% match in pages will boost to 60%. Call this percentage `common_perc`.
    2. The age difference between the matcher and matchee is calculated and hardcapped at 72. It is then divided by 72 to produce a percentage (float) that describes how different their ages are. Call this percentage `age_diff`.
    3. A very special float, called the `age_relation` is calculated. This is a relationship that describes how much weight should be put on age difference at specific ages. When you are matching a 50-year-old and a 55-year old, the age difference matters very little. However when matching an 18-year-old and a 23-year old, that difference matters significantly more. The age_relation helps with this. The float is basically calculated by reading from a very specifically engineered restricted exponential function that is set up to produce 60% at 18 and 30% at 80. These percentages describe the weight of age difference at different ages.
    4. Finally the match quality is calculated by merging likes in common and age difference: 
        - `(common_perc * (100-age_relation)) + (((72-age_diff)/72) * age_relation)`
        - This generates a float percentage which is then hardcapped between 0.0 and 98.0
        - The matching quality assigning algo was created the way it was to allow the percentages to have a value that matches what people intuitively feel they should be. In reality, true soulmates would probably get a 5% mathematical match quality rating, which is why fudging is important. And important to be done correctly. 
        - A lot of the process of matching is done with arbitrary, or approximated values and methods. With machine learning and big data processing, we hope to improve this function to make good matches every time. This is why ratings are important to us.
5. The matchees are order by match quality and limited to the top 5.
6. If there are 3 or more matches, matching is marked complete. However if there aren't, then pseudo-matching begins.
7. Pseudo-matching is a much simpler process than matching, because it merely intends to grab some random people to show to the user in the event that no good matches are found right off the bat. The process entails no more than the following few steps:
    1. Filter out ignored users.
    2. Filter out matches (if any)
    3. Pick 5 random users out of this list, and randomly assign each of them a match quality between 20.0 and 40.0. Not really "close matches", we know.
    - This will happen frequently in early stages of production, where there aren't many registered users. The only case where no matches will be generated at all is if the matcher is the only registered user or he has literally gone through every other registered user. This is sadly possible in the beginning stages of production.

Note: At the current backend version, matching takes quite some time. In a worst-case scenario database (where everyone is a perfect match for everyone else) with 1000 users, matching takes up to 18 seconds. Details about how this is supposed to be handled are at `/matches`.

The old API documentation is available [here](https://nected.stonepillarstudios.com/dev/nected-api-docs.php), but the actual endpoints have been shut down for obvious reasons. Also, I think there's a password in this repo somewhere. It was generated though, so it won't be of much use to anyone who finds it.