<?php
/**
 * Created by PhpStorm.
 * User: koell
 * Date: 13 Mar 2018
 * Time: 6:07 PM
 */

require_once 'utils/funcs.php';

$outputArr = array();

$input = json_decode(file_get_contents("php://input"));

/**
 * input JSON:
 *  action:
 *      ignore / undo_ignore
 *          rauth: string
 *          ignoree: string
 *      rate / rate|ignore
 *          rauth: string
 *          ratee: string
 *          rating: int
 *      report / report|ignore
 *          rauth: string
 *          reportee: string
 *          reasons: string array
 *
 *
 *
 *  note: pids' order doesn't matter
 */

if (isset($input->action) && isset($input->rauth)){

    $profileID = validate_rauth($input->rauth);
    if ($profileID == false) {
        $outputArr['success'] = false;
        $outputArr['failMsg'] = 'BAD_RAUTH';

    }else if ($input->action == 'ignore') {
        add_ignore_couple($profileID, $input->ignoree);
        $outputArr['success'] = true;

    }else if ($input->action == 'undo_ignore'){
        undo_ignore($input->profileID, $input->ignoree);
        $outputArr['success'] = true;

    }else if ($input->action == 'rate') {
        rate_user($profileID, $input->ratee, $input->rating);
        $outputArr['success'] = true;

    }else if ($input->action == 'report'){
        rate_user($profileID, $input->reportee, 0);
        report_user($profileID, $input->reportee, $input->reasons);
        $outputArr['success'] = true;

    }else if ($input->action == 'rate|ignore'){
        rate_user($profileID, $input->ratee, $input->rating);
        add_ignore_couple($profileID, $input->ratee);

        $outputArr['success'] = true;

    }else if ($input->action == 'report|ignore'){
        rate_user($profileID, $input->reportee, 0);
        report_user($profileID, $input->reportee, $input->reasons);
        add_ignore_couple($profileID, $input->reportee);

        $outputArr['success'] = true;

    }else{
        $outputArr['success'] = false;
        $outputArr['failMsg'] = 'Unknown action: ' . $input->action;
    }


}else{
    $outputArr['success'] = false;
    $outputArr['failMsg'] = 'action not set';
}

echo json_encode($outputArr);
$db->close();