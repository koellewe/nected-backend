<?php
/**
 * Created by PhpStorm.
 * User: koell
 * Date: 29 Jan 2018
 * Time: 11:27 AM
 */

require_once 'db.php';

//creates new user if necessary
//also gives user default settings
function insert_user($profileID, $first_name, $last_name, $email, $birthday, $gender){

    global $db;

    $stmt = $db->prepare('CALL create_new_user(?,?,?,?,?,?)');
    $stmt->bind_param('ssssss', $profileID, $first_name, $last_name, $email, $birthday, $gender);
    $stmt->execute();

    $stmt->close();

}

//inserts user's likes into tmp table for later comparisons with likes table.
//please don't send a 1000 item array
//this method also inserts pages into the pages table (if they aren't already)
function insert_likes_tmp($profileID, array $pages){

    global $db;

    $sql_cmp = 'INSERT IGNORE INTO comparisonator (profileID, pageID) VALUES ';
    $sql_pag = 'INSERT IGNORE INTO pages (pageID, pageName) VALUES ';

    for($i = 0; $i < sizeof($pages); $i++){
        $sql_cmp .= '(\'' . $db->real_escape_string($profileID) . '\',\'' . $db->real_escape_string($pages[$i]->id) . '\')' .
            ($i == sizeof($pages)-1 ? '' : ',');
        $sql_pag .= '(\'' . $db->real_escape_string($pages[$i]->id) . '\',\'' . $db->real_escape_string($pages[$i]->name) . '\')' .
            ($i == sizeof($pages)-1 ? '' : ',');
    }

    $db->query($sql_pag);
    $db->query($sql_cmp);

}

//updates likes from tmp table, inserting new ones and deleting missing ones
//call this after inserting likes with insert_likes_tmp()
//this method also automatically adds likes to the pages table, if they aren't already there
//also deletes pages in cmp table afterwards
function update_likes($profileID){

    global $db;

    //insert new pages from cmp into likes
    $stmt = $db->prepare('
INSERT INTO likes(profileID, pageID) ( 
  SELECT profileID, pageID FROM comparisonator
  WHERE comparisonator.profileID = ?
  AND comparisonator.pageID NOT IN (
    SELECT likes.pageID FROM likes
    WHERE likes.profileID = ?
  )
)
    ');
    $stmt->bind_param('ss', $profileID, $profileID);
    $stmt->execute();

    //delete pages not cmp from likes
    $stmt = $db->prepare('
      DELETE FROM likes
      WHERE likes.profileID = ?
      AND likes.pageID NOT IN (
        SELECT comparisonator.pageID FROM comparisonator
        WHERE comparisonator.profileID = ?
      )
    ');
    $stmt->bind_param('ss', $profileID, $profileID);
    $stmt->execute();

    $stmt = $db->prepare('DELETE FROM comparisonator WHERE profileID = ?');
    $stmt->bind_param('s', $profileID);
    $stmt->execute();

    $stmt->close();

}

//generates matches for a single user. In a big set of users, this could take more than 10s.
//internally changes matching_status value
function generate_matches_single($profileID){

    global $db;

    set_time_limit(60*10); //if matching takes longer than 10min, we have a bigger problem at hand
    ignore_user_abort(true);

    $stmt = $db->prepare('CALL generate_matches_single(?)');
    $stmt->bind_param('s', $profileID);
    $stmt->execute();
    $stmt->close();

}

//just gets pre-rendered matches (for efficiency's sake)
function get_matches($profileID){

    global $db;

    $stmt = $db->prepare('SELECT matchee, first_name, last_name, FLOOR(DATEDIFF(NOW(), DoB)) AS age,
                              matches.quality, messengerUsername, matches.legit_match
                            FROM users INNER JOIN matches ON users.profileID = matches.matchee
                            WHERE matches.matcher = ?
                            ORDER BY quality DESC');
    $stmt->bind_param('s', $profileID);

    $stmt->execute();

    $stmt->bind_result($proID, $firstname, $lastname, $age, $quality, $messengerUsername, $legitimacy);

    $matches = array();
    $i = 0;
    while ($stmt->fetch()){

        $match = new \stdClass();
        $match->profileID = $proID;
        $match->first_name = $firstname;
        $match->last_name = $lastname;
        $match->age = $age;
        $match->quality = $quality;
        $match->messengerUsername = $messengerUsername;
        $match->legitimacy = $legitimacy == true ? 1 : 0;

        $matches[$i] = $match;
        $i++;

    }

    $stmt->close();

    return $matches;

}

//gets the min amount of page matches needed to fulfill match_quality
function get_total_pages($profileID){

    global $db;

    $stmt = $db->prepare('SELECT COUNT(pageID) FROM likes WHERE profileID = ?');
    $stmt->bind_param('s', $profileID);
    $stmt->execute();
    $stmt->bind_result($total);
    $stmt->fetch();

    $stmt->close();
    return $total;

}

function get_tbl_len($tbl, $profileID){
    global $db;

    $stmt = $db->prepare("SELECT COUNT(*) FROM $tbl WHERE profileID = ?");
    $stmt->bind_param('s', $profileID);
    $stmt->execute();
    $stmt->bind_result($total);
    $stmt->fetch();

    $stmt->close();
    return $total;

}

function get_settings($profileID){

    global $db;

    $stmt = $db->prepare('SELECT setting_key, setting_value FROM user_settings WHERE profileID = ?');
    $stmt->bind_param('s', $profileID);
    $stmt->execute();
    $stmt->bind_result($key, $val);

    $settings = array();
    $i = 0;
    while($stmt->fetch()){
        $setting = new \stdClass();
        $setting->setting_key = $key;
        $setting->setting_value = $val;
        $settings[$i++] = $setting;
    }

    $stmt->close();
    return $settings;

}

function save_settings($profileID, array $settings){

    global $db;

    $stmt = $db->prepare('UPDATE user_settings SET setting_value = ? WHERE profileID = ? AND setting_key = ?');

    for($i = 0; $i < sizeof($settings); $i++){
        $stmt->bind_param('sss', $settings[$i]->setting_value, $profileID, $settings[$i]->setting_key);
        $stmt->execute();
    }

    $stmt->close();

}

function get_setting($profileID, $setting_key){

    global $db;

    $stmt = $db->prepare('SELECT setting_value FROM user_settings WHERE profileID = ? AND setting_key = ?');
    $stmt->bind_param('ss', $profileID, $setting_key);
    $stmt->execute();
    $stmt->bind_result($setting_value);
    $stmt->fetch();

    $stmt->close();
    return $setting_value;

}

function user_exists($profileID){

    global $db;

    $stmt = $db->prepare('SELECT profileID FROM users WHERE profileID = ?');
    $stmt->bind_param('s', $profileID);
    $stmt->execute();

    $exists = $stmt->fetch() == true;

    $stmt->close();

    return $exists;
}

//updates the locations table with a user's new location (long+lat)
function new_loc($profileID, $x, $y){

    global $db;

    $stmt = $db->prepare('INSERT INTO locations (profileID, coords) VALUES (?, POINT(?,?))');
    $stmt->bind_param('sdd', $profileID, $x, $y);
    $stmt->execute();

    $stmt->close();
}

function add_ignore_couple($ignorer, $ignoree){

    global $db;

    $stmt = $db->prepare('CALL add_ignore_couple(?, ?)');
    $stmt->bind_param('ss', $ignorer, $ignoree);
    $stmt->execute();

    $stmt->close();

}

function undo_ignore($ignorer, $ignoree){

    global $db;

    $stmt = $db->prepare('CALL undo_ignore(?, ?)');
    $stmt->bind_param('ss', $ignorer, $ignoree);
    $stmt->execute();

    $stmt->close();

}

//adds default settings for the profileID if they have not already been set
//this is used when new settings are added. Do not remove!
function add_new_default_settings($profileID){

    global $db;

    $result = $db->query('SELECT setting_key, default_value FROM default_settings');

    while (($def = $result->fetch_assoc()) != null){

        $stmt = $db->prepare('INSERT INTO user_settings(profileID, setting_key, setting_value) VALUES (?,?,?)');
        $stmt->bind_param('sss', $profileID, $def['setting_key'], $def['default_value']);
        $stmt->execute();
        $stmt->close();

    }

}

//sets or updates messengerUsername for profileID
function set_messenger($profileID, $messengerUsername){

    global $db;

    $stmt = $db->prepare('UPDATE users SET messengerUsername = ? WHERE profileID = ?');
    $stmt->bind_param('ss', $messengerUsername, $profileID);
    $stmt->execute();
    $stmt->close();

}

//returns an array with details on whether the user is ready to start matching
//and if not, why
function get_user_readiness($profileID){

    global $db;

    $user_readiness = array();
    $user_readiness['ready'] = true;

    $stmt = $db->prepare('SELECT messengerUsername, DoB, gender FROM users WHERE profileID = ?');
    $stmt->bind_param('s', $profileID);
    $stmt->execute();
    $stmt->bind_result($mesUsr, $dob, $gender);
    $stmt->fetch();

    $user_readiness['issues_array'] = array();

    if ($mesUsr == null){
        $user_readiness['ready'] = false;
        array_push($user_readiness['issues_array'], 'MESSENGER_USERNAME_NOT_SET');
    }

    if ($dob == null){
        $user_readiness['ready'] = false;
        array_push($user_readiness['issues_array'], 'DOB_NOT_SET');
    }

    if ($gender == 'O'){
        $user_readiness['ready'] = false;
        array_push($user_readiness['issues_array'], 'GENDER_NOT_SET');
    }

    $stmt->close();

    if (get_total_pages($profileID) < MINIMUM_LIKES){
        $user_readiness['ready'] = false;
        array_push($user_readiness['issues_array'], 'TOO_FEW_LIKES');
    }

    $loc_stmt = $db->prepare('SELECT profileID FROM locations WHERE profileID = ?');
    $loc_stmt->bind_param('s', $profileID);
    $loc_stmt->execute();

    if (!$loc_stmt->fetch()){
        $user_readiness['ready'] = false;
        array_push($user_readiness['issues_array'], 'NO_LOCATION');
    }

    $loc_stmt->close();


    return $user_readiness;

}

function set_user_registered($profileID, $registered){

    global $db;

    $stmt = $db->prepare('UPDATE users SET registered = ? WHERE profileID = ?');
    $stmt->bind_param('is', $registered, $profileID);
    $stmt->execute();

    $stmt->close();

}

function get_user_info($profileID){

    global $db;

    $stmt = $db->prepare('SELECT first_name, last_name, email, DoB, gender, messengerUsername, registered FROM users WHERE profileID = ?');
    $stmt->bind_param('s', $profileID);
    $stmt->execute();
    $stmt->bind_result($fname, $lname, $email, $dob, $gender, $mess, $reg);
    $stmt->fetch();

    $user = new \stdClass();
    $user->first_name = $fname;
    $user->last_name = $lname;
    $user->email = $email;
    $user->DoB = $dob;
    $user->gender = $gender;
    $user->messengerUsername = $mess;
    $user->registered = $reg == 1;

    return $user;

}

//returns true if user registration marked as complete
function is_registered($profileID){

    global $db;

    $stmt = $db->prepare('SELECT registered FROM users WHERE profileID = ?');
    $stmt->bind_param('s', $profileID);
    $stmt->execute();
    $stmt->bind_result($reg);
    $stmt->fetch();

    $stmt->close();
    return $reg == 1;

}

//adds user rating to ratings table
//if rater has already rated ratee, nothing happens.
function rate_user($rater, $ratee, $rating){

    global $db;

    $stmt = $db->prepare('INSERT INTO ratings(rater, ratee, rating) VALUES (?,?,?)');
    $stmt->bind_param('ssi', $rater, $ratee, $rating);
    $stmt->execute();

    $stmt->close();

}

//adds report from user of another user.
//reasons must be a string array
function report_user($reporter, $reportee, array $reasons){

    global $db;

    $reasons_str = json_encode($reasons);

    $stmt = $db->prepare('INSERT INTO reports(reporter, reportee, reasons) VALUES (?,?,?)');
    $stmt->bind_param('sss', $reporter, $reportee, $reasons_str);
    $stmt->execute();
    $stmt->close();

}

//checks whether given rauth is legit
//returns corresponding profileID if it is
//returns false if it isn't
function validate_rauth($rauth){

    global $db;

    $stmt = $db->prepare('SELECT profileID FROM rauths WHERE rauth = ?');
    $stmt->bind_param('s', $rauth);
    $stmt->execute();

    $stmt->bind_result($profileID);

    if ($stmt->fetch()){

        $stmt->close();
        return $profileID;

    }else{
        $stmt->close();
        return false;
    }

}

//gets rauth for profileID, generates if it does not exist yet
function get_rauth($profileID){

    global $db;

    $stmt = $db->prepare('SELECT rauth FROM rauths WHERE profileID = ?');
    $stmt->bind_param('s', $profileID);
    $stmt->execute();

    $stmt->bind_result($rauth_r);

    if ($stmt->fetch()){

        $stmt->close();
        return $rauth_r;

    }else{
        $stmt->close();

        $rauth = password_hash($profileID.time(), PASSWORD_BCRYPT);

        $stmt2 = $db->prepare('INSERT INTO rauths (rauth, profileID) VALUES (?,?)');
        $stmt2->bind_param('ss', $rauth, $profileID);
        $stmt2->execute();

        $stmt2->close();

        return $rauth;

    }
}

//deletes rauth of a profileID
function del_rauth($profileID){

    global $db;

    $stmt = $db->prepare('DELETE FROM rauths WHERE profileID = ?');
    $stmt->bind_param('s', $profileID);
    $stmt->execute();

    $stmt->close();

}

//returns whether the system is currently generating matches for the profileID
function is_matching($profileID){

    global $db;

    $stmt = $db->prepare('SELECT status FROM matching_statuses WHERE profileID = ?');
    $stmt->bind_param('s', $profileID);
    $stmt->execute();

    $stmt->bind_result($status);

    $fetched = $stmt->fetch();
    $stmt->close();

    return $fetched ? $status : false;

}