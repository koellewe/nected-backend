DROP FUNCTION IF EXISTS gender_matches;
CREATE FUNCTION gender_matches(matcherPID VARCHAR(255), matcheePID VARCHAR(255))
  RETURNS TINYINT(1)
  BEGIN

    DECLARE likes_men TINYTEXT DEFAULT
      (SELECT setting_value FROM user_settings
      WHERE profileID = matcherPID AND setting_key = 'LIKES_MEN'
       LIMIT 1);
    DECLARE likes_men_bool BOOLEAN DEFAULT
      UPPER(likes_men) = 'TRUE';

    DECLARE likes_women TINYTEXT DEFAULT
      (SELECT setting_value FROM user_settings
      WHERE profileID = matcherPID AND setting_key = 'LIKES_WOMEN'
       LIMIT 1);
    DECLARE likes_women_bool BOOLEAN DEFAULT
      UPPER(likes_women) = 'TRUE';

    DECLARE matcheeGender CHAR DEFAULT
      (SELECT gender FROM users
      WHERE profileID = matcheePID);

    RETURN (likes_men_bool and matcheeGender = 'M') OR
           (likes_women_bool and matcheeGender = 'F');

  END;
