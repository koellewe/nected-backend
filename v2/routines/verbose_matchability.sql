DROP PROCEDURE IF EXISTS verbose_matchability;
CREATE PROCEDURE verbose_matchability(IN matcherPID VARCHAR(255), IN matcheePID VARCHAR(255))
  BEGIN

    DECLARE min_quality INTEGER DEFAULT
      get_setting(matcherPID, 'MATCH_QUALITY');

    DECLARE min_age INTEGER DEFAULT
      get_setting(matcherPID, 'MIN_AGE');

    DECLARE max_age INTEGER DEFAULT
      get_setting(matcherPID, 'MAX_AGE');

    DECLARE max_radius DOUBLE DEFAULT
      get_max_radius(matcherPID);

    DECLARE page_matches INTEGER DEFAULT 0;
    DECLARE ignore_count INTEGER DEFAULT 0;
    DECLARE matcheeAge INTEGER DEFAULT 0;

    DECLARE matcherLoc POINT DEFAULT
      (SELECT coords
       FROM locations
       WHERE profileID = matcherPID
       ORDER BY update_timestamp DESC
       LIMIT 1);

    DECLARE matcheeLoc POINT DEFAULT
      (SELECT coords
       FROM locations
       WHERE profileID = matcheePID
       ORDER BY update_timestamp DESC
       LIMIT 1);

    DECLARE actual_distance DOUBLE DEFAULT 0.0;

    DECLARE likes_men TINYTEXT DEFAULT
      get_setting(matcherPID, 'LIKES_MEN');

    DECLARE likes_women TINYTEXT DEFAULT
      get_setting(matcherPID, 'LIKES_WOMEN');

    DECLARE likes_str TINYTEXT DEFAULT '';

    DECLARE matcheeGender CHAR DEFAULT '';

    DECLARE is_registered BOOLEAN DEFAULT FALSE;

    IF max_age >= 80 THEN
      SET max_age = 9999;
    END IF;

    DROP TEMPORARY TABLE IF EXISTS verbose_out;
    CREATE TEMPORARY TABLE verbose_out (id INT PRIMARY KEY AUTO_INCREMENT, category TINYTEXT, requirement TINYTEXT, reality TINYTEXT);


    INSERT INTO verbose_out (category, requirement, reality) VALUES ('match_quality', min_quality, matchability(matcherPID, matcheePID));

    SELECT COUNT(ignoree) INTO ignore_count
    FROM ignores WHERE ignorer = matcherPID
                       AND ignoree = matcheePID;

    INSERT INTO verbose_out (category, requirement, reality) VALUES ('ignored', '0', ignore_count);

    SELECT FLOOR(DATEDIFF(NOW(), users.DoB) / 365.25) INTO matcheeAge
    FROM users WHERE profileID = matcheePID;

    INSERT INTO verbose_out(category, requirement, reality) VALUES ('age', CONCAT(min_age, ' - ', max_age), matcheeAge);

    SET actual_distance = (
      6371 *
      acos(
          cos( radians( Y(matcherLoc) ) ) *
          cos( radians( Y(matcheeLoc) ) ) *
          cos(
              radians( X(matcheeLoc) ) - radians( X(matcherLoc) )
          ) +
          sin(radians(Y(matcherLoc))) *
          sin(radians(Y(matcheeLoc)))
      )
    );

    INSERT INTO verbose_out(category, requirement, reality) VALUES ('distance', max_radius, actual_distance);

    IF likes_men = 'True' AND likes_women = 'True' THEN
      SET likes_str = 'M,F';
    ELSE IF likes_men = 'True' THEN
      SET likes_str = 'M';
    ELSE
      SET likes_str = 'F';
    END IF;
    END IF;

    SELECT gender INTO matcheeGender
    FROM users
    WHERE profileID = matcheePID;

    INSERT INTO verbose_out(category, requirement, reality) VALUES ('gender', likes_str, matcheeGender);

    SELECT registered INTO is_registered
    FROM users WHERE profileID = matcheePID;

    INSERT INTO verbose_out(category, requirement, reality) VALUES ('registered', '1', is_registered);

    INSERT INTO verbose_out(category, requirement, reality) VALUES ('is_matchable', '', is_matchable(matcherPID, matcheePID));

    SELECT * FROM verbose_out;

  END;
