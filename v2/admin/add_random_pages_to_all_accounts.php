<?php
/**
 * Created by PhpStorm.
 * User: koell
 * Date: 06 Feb 2018
 * Time: 9:49 AM
 */

require_once '../utils/db.php';
require_once '../utils/funcs.php';

function get_random_pageID($max){

    global $db;

    $random = mt_rand(0, $max-1);

    $page_q = $db->query('SELECT pageID FROM pages LIMIT '.$random . ', ' . ($random+1));

    return $page_q->fetch_assoc()['pageID'];

}

if ($_GET['admin_auth'] != 'Vu2xJCGzLAy4'){
    echo '<h1>ERROR: bad auth</h1>';
    exit();
}

$result = $db->query('SELECT profileID FROM users');

if ($result == false){
    echo 'Something went wrong.';
}else{

    echo '<h1>Likes generated for:</h1><ul>';

    $likes_len_q = $db->query('SELECT COUNT(pageID) AS count FROM pages');
    $likes_len_r = $likes_len_q->fetch_assoc();

    $total_pages = $likes_len_r['count'];

    $likes_generated = 0;

    while (($user = $result->fetch_assoc()) != null){

        $sub_likes_gen = 0;

        $pages_to_add = 50 - get_total_pages($user['profileID']);

        while ($pages_to_add > $sub_likes_gen){

            $db->query("INSERT INTO likes(profileID, pageID) VALUES ('".$user['profileID']."','".
                get_random_pageID($total_pages) ."')"
            );
            //note: this insert is prone to dupe-fails, but the like minimum is pretty much guaranteed, given the total pages count.

            $likes_generated++;
            $sub_likes_gen++;

        }

        if ($sub_likes_gen > 0)
            echo '<li>'.$user['profileID'].' - '.$sub_likes_gen.' new likes</li>';

    }

    echo '</ul><p>Done. ' . $likes_generated . ' new likes added.</p>';

}

$db->close();
