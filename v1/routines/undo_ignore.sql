DROP PROCEDURE IF EXISTS undo_ignore;
CREATE PROCEDURE undo_ignore(IN pIgnorer VARCHAR(255), IN pIgnoree VARCHAR(255))
  BEGIN

    DELETE FROM ignores
    WHERE
      (ignorer = pIgnorer AND ignoree = pIgnoree)
      OR
      (ignorer = pIgnoree AND ignoree = pIgnorer);

  END;
