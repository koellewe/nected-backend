DROP PROCEDURE IF EXISTS get_matches;
CREATE PROCEDURE get_matches(IN reqPID VARCHAR(32))
  BEGIN

    DECLARE min_quality INTEGER DEFAULT
      get_setting(reqPID, 'MATCH_QUALITY');

    SELECT profileID, first_name, last_name, age, match_quality, messengerUsername
    FROM (

      SELECT
        profileID,
        first_name,
        last_name,
        age,
        matchability(reqPID, profileID, like_matches_num) as match_quality,
        messengerUsername
      FROM (

             SELECT
               profileID,
               first_name,
               last_name,
               age,
               like_matches_num,
               messengerUsername
             FROM (

                    SELECT
                      likes.profileID,
                      first_name,
                      last_name,
                      age,
                      messengerUsername,
                      COUNT(likes.profileID) as like_matches_num
                    FROM likes
                      INNER JOIN

                      (

                        SELECT
                          profileID,
                          first_name,
                          last_name,
                          age,
                          messengerUsername
                        FROM
                          (
                            SELECT
                              profileID,
                              first_name,
                              last_name,
                              age,
                              messengerUsername
                            FROM
                              (
                                SELECT
                                  profileID,
                                  first_name,
                                  last_name,
                                  FLOOR(DATEDIFF(NOW(), DoB) / 365.25) AS age,
                                  messengerUsername

                                FROM users

                                WHERE profileID <> reqPID

                                      AND profileID NOT IN (

                                  SELECT ignoree
                                  FROM ignores
                                  WHERE ignorer = reqPID

                                )) as tbl_unignored

                            WHERE is_matchable(reqPID, profileID)

                          ) as tbl_match_forward

                        WHERE is_matchable(profileID, reqPID)

                      ) as tbl_match_backward

                        ON tbl_match_backward.profileID = likes.profileID

                    WHERE pageID IN (
                      SELECT pageID
                      FROM likes
                      WHERE profileID = reqPID
                    )
                    GROUP BY profileID

                  ) as tbl_matchees_raw

           ) as tbl_matchability

    ) as tbl_matchability_2

    WHERE match_quality > min_quality

    ORDER BY match_quality DESC LIMIT 5;


    END;